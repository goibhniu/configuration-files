(require 'package)
(add-to-list 'package-archives 
    '("marmalade" .
      "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)
;; This installs the list of packages automatically from marmalade/elpa on startup
(require 'cl)
 
(defvar my-packages
  '(ack-and-a-half buffer-move fastnav json paredit rainbow-mode)
  "A list of packages to ensure are installed at launch.")

(defun my-packages-installed-p ()
  (loop for p in my-packages
        when (not (package-installed-p p)) do (return nil)
        finally (return t)))
 
(unless (my-packages-installed-p)
  ;; check for new packages (package versions)
  (package-refresh-contents)
  ;; install the missing packages
  (dolist (p my-packages)
    (when (not (package-installed-p p))
      (package-install p))))

;; http://www.djcbsoftware.nl/dot-emacs.html
(defconst manually-installed '("~/7rl/configuration-files/emacs/modules"))
(mapcar '(lambda(p)
           (add-to-list 'load-path p)
           (cd p) (normal-top-level-add-subdirs-to-load-path))
        manually-installed)

(defconst manually-installed2 '("~/.emacs.d/manually-installed/emacs-flymake"))
(mapcar '(lambda(p)
           (add-to-list 'load-path p)
           (cd p) (normal-top-level-add-subdirs-to-load-path))
        manually-installed2)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; General / Appearance

;; monospace font
(set-face-attribute 'default nil :font "Inconsolata" :height 120)
;; bold also sets the background to grey, setting to white instead
(set-face-background 'bold "light grey")
;; enable bash color for shell
(add-hook 'shell-mode-hook
	  'ansi-color-for-comint-mode-on)

(load-theme 'solarized-light t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Inconsolata" :foundry "unknown" :slant normal :weight normal :height 116 :width normal)))))

;; Scroll bars on the right
(setq scroll-bar-mode-explicit t)
(set-scroll-bar-mode `right)

;; Hide the tool bar
(tool-bar-mode -1)

;; instead of a beep
(setq visible-bell t)

(line-number-mode 1)
(global-linum-mode 1)
(column-number-mode 1)

;; Copy and paste with Xorg
(setq x-select-enable-primary 't)

;; auto-complete
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/manually-installed//ac-dict")
(ac-config-default)

;; Turn off all tabs :(
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq auto-fill-mode 1)
;; (add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq-default ispell-program-name "aspell")
(setq ispell-personal-dictionary "~/.ispell-dict-personal")
(setq ispell-dictionary "american")

;; Develop and keep personal snippets under 7rl
(setq yas-snippet-dirs '("~/7rl/configuration-files/emacs/snippets"))
(yas-global-mode 1)

;; http://yenliangl.blogspot.com/2009/05/emacs-session-manager-winring.html
(require 'winring)
(winring-initialize)
(setq winring-show-names t)

(desktop-save-mode 1)
(add-hook 'auto-save-hook (lambda () (desktop-save-in-desktop-dir)))

;; only use one dired buffer
(put 'dired-find-alternate-file 'disabled nil)

(setq gnus-button-url 'browse-url-generic
      browse-url-generic-program "firefox"
      browse-url-browser-function gnus-button-url)

;; Add Emacs close confirmation
(setq kill-emacs-query-functions
      (cons (lambda () (yes-or-no-p "Kill Emacs? U SRS?"))
            kill-emacs-query-functions))

(recentf-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File navigation

;; tramp
(require 'tramp)
(setq tramp-default-method "ssh")
(setq tramp-debug-buffer t)
(setq tramp-verbose 9)
(setq tramp-persistency-file-name nil)
(setq password-cache-expiry nil)
;; For tramp to find software on NixOS:
(add-to-list 'tramp-remote-path "/var/run/current-system/sw/bin")
;; Do save tramp files with desktop
(setq desktop-files-not-to-save "^$")

;; Attempt to get rgrep to also find files via symlinks
;; original
;; ("find . -type f -exec grep -nH -e  {} +" . 34)
;; (grep-apply-setting 'grep-find-command "(\"find -L . -type f -exec grep -nH -e {} +\" . 34)")

;; http://www.gnu.org/software/tramp/
;; Show hostname
(defconst my-mode-line-buffer-identification
  (list
   '(:eval
     (let ((host-name
            (or (file-remote-p default-directory 'host)
                (system-name))))
       (if (string-match "^[^0-9][^.]*\\(\\..*\\)" host-name)
           (substring host-name 0 (match-beginning 1))
         host-name)))
   ": %12b"))

;; http://emacs-fu.blogspot.de/2011/01/setting-frame-title.html
(setq frame-title-format
  '("" invocation-name ": "(:eval (if (buffer-file-name)
                (abbreviate-file-name (buffer-file-name))
                  "%b"))))

(setq-default
 mode-line-buffer-identification
 my-mode-line-buffer-identification)

(add-hook
 'dired-mode-hook
 '(lambda ()
    (setq
     mode-line-buffer-identification
     my-mode-line-buffer-identification)))


;; Show path info in otherwise identical filenames
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward)

;; remember last place in each file
(setq-default save-place t)
(require 'saveplace)

;; bookmarks http://www.io.com/~jimm/emacs_tips.html setting a
;; bookmark is C-x r m BOOKMARK <RET> (where BOOKMARK is a name for
;; which you are prompted in the mini-buffer). For jumping to a
;; bookmark, the default key binding is C-x r b BOOKMARK <RET>. C-x r
;; l
(define-key global-map [f9] 'bookmark-jump)
(define-key global-map [f10] 'bookmark-set)

(setq bookmark-save-flag 1)		; How many mods between saves

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Text navigation

;; fastnav http://www.emacswiki.org/emacs/FastNav
(require 'fastnav)

(global-set-key "\M-z" 'fastnav-zap-up-to-char-forward)
(global-set-key "\M-Z" 'fastnav-zap-up-to-char-backward)
(global-set-key "\M-s" 'fastnav-jump-to-char-forward)
(global-set-key "\M-S" 'fastnav-jump-to-char-backward)
(global-set-key "\M-r" 'fastnav-replace-char-forward)
(global-set-key "\M-R" 'fastnav-replace-char-backward)
(global-set-key "\M-i" 'fastnav-insert-at-char-forward)
(global-set-key "\M-I" 'fastnav-insert-at-char-backward)
(global-set-key "\M-j" 'fastnav-execute-at-char-forward)
(global-set-key "\M-J" 'fastnav-execute-at-char-backward)
(global-set-key "\M-k" 'fastnav-delete-char-forward)
(global-set-key "\M-K" 'fastnav-delete-char-backward)
(global-set-key "\M-m" 'fastnav-mark-to-char-forward)
(global-set-key "\M-M" 'fastnav-mark-to-char-backward)

(global-set-key "\M-p" 'fastnav-sprint-forward)
(global-set-key "\M-P" 'fastnav-sprint-backward)

;; http://emacs.wordpress.com/2007/01/16/quick-and-dirty-code-folding/
(defun jao-selective-display ()
  (interactive)
  (set-selective-display;; TODO: debug

   (if selective-display
       nil
     (+ 1 (current-column)))))
(global-set-key [f1] 'jao-selective-display)

;; set mode line to show full path of current file
;; (setq-default mode-line-format
;;    (list '((buffer-file-name " %f"
;;               (dired-directory
;;                dired-directory
;;                 (revert-buffer-function " %b"
;;                ("%b - Dir:  " default-directory)))))))

(winner-mode 1)

(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

(global-set-key (kbd "s-c <left>")  'windmove-left)
(global-set-key (kbd "s-c <right>") 'windmove-right)
(global-set-key (kbd "s-c <up>")    'windmove-up)
(global-set-key (kbd "s-c <down>")  'windmove-down)

(require 'buffer-move)

;; http://xahlee.org/emacs/file_management.html
;; make enter open the next dir in the same buffer
(add-hook 'dired-mode-hook
 (lambda ()
  (define-key dired-mode-map (kbd "<return>")
    'dired-find-alternate-file) ; was dired-advertised-find-file
  (define-key dired-mode-map (kbd "^")
    (lambda () (interactive) (find-alternate-file "..")))
  ; was dired-up-directory
 ))

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))
;; also don't create .# links
(setq create-lockfiles nil)

(icomplete-mode t)

;; emacsrocks!
(require 'expand-region)
(global-set-key (kbd "C-#") 'er/expand-region)

;; (require 'inline-string-rectangle)
;; (global-set-key (kbd "C-x r t") 'inline-string-rectangle)
 
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; mode hooks

(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(setq whitespace-line-column 80)
(add-hook 'python-mode-hook
          '(lambda ()
             (set-fill-column 80)
             (linum-mode t)
             (whitespace-mode t)
             (show-paren-mode t)
             (flymake-mode t)
             )
          t)

;; (add-hook 'rst-mode-hook
;;           '(lambda ()
;;              (flyspell-mode f)
;;              (flyspell-buffer f)
;;              )
;;           t)

(add-hook 'xml-mode-hook
          '(lambda ()
             (linum-mode t)
             (whitespace-mode t)
             )
          t)

(load-library "magit")
(add-hook 'magit-log-edit-mode-hook
          '(lambda ()
             (flyspell-mode t)
             (flyspell-buffer t)
             )
          t)

(add-hook 'markdown-mode-hook
          '(lambda ()
             (set-fill-column 80)
             (flyspell-mode t)
             (flyspell-buffer t)
             )
          t)

(add-hook 'php-mode-hook
          '(lambda ()
             (linum-mode t)
             (setq tab-width 4)
             (setq c-basic-offset 4)
             ;; (setq indent-tabs-mode t)
             )
          t)

;; (add-hook 'log-edit-mode-hook
;;           '(lambda ()
;;              (flyspell-mode t)
;;              (flyspell-buffer t)
;;              )
;;           t)

;; http://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml
(setq-default css-indent-offset 2)
(add-hook 'css-mode-hook
          '(lambda ()
             (linum-mode t)
             (whitespace-mode t)
             (show-paren-mode t)
             )
          t)

;; (require 'rename-sgml-tag)
;; (add-hook 'sgml-mode-hook
;;           '(lambda ()
;;              (linum-mode t)
;;              (whitespace-mode t)
;;              (setq indent-tabs-mode t)
;;              (define-key sgml-mode-map (kbd "C-c C-r") 'rename-sgml-tag)
;;              )
;;           t)

;;(org-indent-mode t)
;; alternative to org-indent-mode:
;; (adaptive-wrap-prefix-mode)

;; (add-hook 'org-mode-hook
;;           '(lambda ()
;;              (org-replace-disputed-keys t)
;;              )
;;           t)

;; (setq org-replace-disputed-keys t)

(setq js-indent-level 4)
;; mrud's org time tracker stuff
;; (define-key global-map "\C-cj" 'org-capture)
;; (setq org-capture-templates
;;       '(("t" "Todo" entry (file+headline "~/org/tasks.org" "Tasks")
;;          "* TODO %?\n  %i\n  %a")
;;         ("j" "Journal ucd" entry (file+datetree "~/Desktop/ucd/journal/journal.org")
;;          "* %?\nEntered on %U\n  %i\n")))

(add-hook 'javascript-mode-hook
          '(lambda ()
             (linum-mode t)
             (whitespace-mode t)
             (rainbow-parenmode)
             )
          t)
             ;; (setq indent-tabs-mode t)

             ;; (flymake-mode t)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Python and related tools

;; Tried the following to get autocompletion to work with yasnippet and rope
;; https://github.com/EnigmaCurry/emacs/blob/master/ryan-python.el

;; https://github.com/illusori/emacs-flymake
;; Nope, I want my copies in the system temp dir.
(setq flymake-run-in-place nil)
;; This lets me say where my temp dir is.
(setq temporary-file-directory "~/.emacs.d/tmp/")

(setq jedi:setup-keys t)
;; http://www.cis.upenn.edu/~edloper/projects/doctestmode/
(add-to-list 'auto-mode-alist '("\\.doctest$" . doctest-mode))
(autoload 'doctest-mode "doctest-mode" "doctest mode" t)

;; code checking via flymake
;; set code checker here from "epylint", "pyflakes"
(setq python-check-command "flake8")

(when (load "flymake" t)
  (defun flymake-pyflakes-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
               'flymake-create-temp-copy))
       (local-file (file-relative-name
            temp-file
            (file-name-directory buffer-file-name))))
      (list "flake8"  (list local-file))))
   (add-to-list 'flymake-allowed-file-name-masks
             '("\\.py\\'" flymake-pyflakes-init)))

(load-library "flymake-cursor")

;; Pymacs
;; (autoload 'pymacs-apply "pymacs")
;; (autoload 'pymacs-call "pymacs")
;; (autoload 'pymacs-eval "pymacs" nil t)
;; (autoload 'pymacs-exec "pymacs" nil t)
;; (autoload 'pymacs-load "pymacs" nil t)
;; (setq pymacs-auto-restart 'ask)

;; ;; From ropemacs README.txt
;; (require 'pymacs)
;; (pymacs-load "ropemacs" "rope-")



;; From http://slacy.com/blog/2011/03/pymacs-ropemacs-and-virtualenv-all-at-the-same-time/
;; Doesn't work for my plone venvs because I use them through a chroot too
;; (setq virtual-env (getenv "VIRTUAL_ENV"))
;; (setq load-path (append
;;                  (list (concat virtual-env "/src/pymacs" ))
;;                  load-path))

;; (if (not (equal virtual-env 'nil))
;;     (let ((foo 'bar))
;;       (require 'pymacs)
;;       (pymacs-load "ropemacs" "rope-")
;;       (setq ropemacs-enable-autoimport 't)
;;       ))

;; (elpy-enable)

(global-set-key [f8]      'flymake-mode)
(global-set-key [f9]      'python-mode)
(global-set-key [f10]      'doctest-mode)


;; for jedi ... I think
(setenv "PYTHONPATH" "/run/current-system/sw/lib/python2.7/site-packages/")


;; http://msnyder.info/posts/2010/10/modular-el-get-pymacs-recipes/
;; (defun add-to-pythonpath (path)
;;   "Adds a directory to the PYTHONPATH environment
;; variable. Automatically applies expand-file-name to `path`."
;;   (setenv "PYTHONPATH"
;;       (concat (expand-file-name path) ":" (getenv "PYTHONPATH"))))

;; (add-to-pythonpath "/var/run/current-system/sw/lib/python2.6/site-packages")
;; (setq py-python-command "/mnt/debian/home/cillian/Syslab/OSHA/bin/zopepy")

;; (setq ipython-command "/usr/bin/ipython")
;; (require 'anything-ipython)
;; (global-set-key [(f5)] 'ipython-complete)

;; pysmell
;;(require 'pysmell)
;;(add-hook 'python-mode-hook (lambda () (pysmell-mode 1)))

;; to always have flymake on (I think)
;; (add-hook 'find-file-hook 'flymake-find-file-hook)

;; The version of rst.el I have assumes the command is called
;; rst2html.py, Overriding with `rst2html`
;; (setcdr (assq 'html rst-compile-toolsets)
;;       '("rst2html" ".html" nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; JavaScript

(autoload 'javascript-mode "javascript" nil t)
(setq auto-mode-alist (append '(("\\.js$" . javascript-mode))
                              auto-mode-alist))
;(require 'flymake-jslint)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Templating languages

;; nxml-mode html5 support
(add-to-list 'load-path "~/.emacs.d/manually-installed/html5-el")
(eval-after-load "rng-loc"
  '(add-to-list 'rng-schema-locating-files "~/.emacs.d/manually-installed/html5-el/schemas.xml"))
(require 'whattf-dt)

;; html-mode for dtml/zpt
(add-to-list 'auto-mode-alist '("\\.zpt$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.pt$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.zcml$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.dtml$" . css-mode))

(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))

(add-to-list 'auto-mode-alist '("\\.py$" . python-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MediaWiki php
;;; http://www.mediawiki.org/wiki/Manual:Coding_conventions#emacs_style
(defconst mw-style
  '((indent-tabs-mode . t)
    (tab-width . 4)
    (c-basic-offset . 4)
    (c-offsets-alist . ((case-label . +)
                        (arglist-cont-nonempty . +)
                        (arglist-close . 0)
                        (cpp-macro . (lambda(x) (cdr x)))
                        (comment-intro . 0)))
    (c-hanging-braces-alist
        (defun-open after)
        (block-open after)
        (defun-close))))

(c-add-style "MediaWiki" mw-style)

(define-minor-mode mah/mw-mode
  "tweak style for mediawiki"
  nil " MW" nil
  (tabify (point-min) (point-max))
  (subword-mode 1))

;; Add other sniffers as needed
(defun mah/sniff-php-style (filename)
  "Given a filename, provide a cons cell of
   (style-name . function)
where style-name is the style to use and function
sets the minor-mode"
  (cond ((string-match "/\\(mw[^/]*\\|mediawiki\\)/"
                       filename)
         (cons "MediaWiki" 'mah/mw-mode))
        (t
         (cons "cc-mode" (lambda (n) t)))))

(add-hook 'php-mode-hook (lambda () (let ((ans (when (buffer-file-name)
                                                 (mah/sniff-php-style (buffer-file-name)))))
                                      (c-set-style (car ans))
                                      (funcall (cdr ans) 1))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; org-mode
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
(setq org-replace-disputed-keys t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Misc

;; (require 'undo-tree)
;; (global-undo-tree-mode)

;; tags
(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (eshell-command
   (format "find -L %s -type f -iregex '.*\\.\\(py\\|css\\|xml\\|dtml\\)' | etags -" dir-name)))

;;erlang-mode
;; (setq erlang-root-dir "/run/current-system/sw")
;; (setq exec-path (cons "/run/current-system/sw/bin" exec-path))
;; (require 'erlang-start)

;; (add-to-list 'load-path "~/7rl/configuration-files/emacs/modules/distel/elisp")
;; (require 'distel)
;; (distel-setup)

;; ;; http://bc.tech.coop/blog/070528.html
;; ;; Some Erlang customizations
;; (add-hook 'erlang-mode-hook
;; 	  (lambda ()
;; 	    ;; when starting an Erlang shell in Emacs, default in the node name
;; 	    (setq inferior-erlang-machine-options '("-sname" "emacs"))
;; 	    ;; add Erlang functions to an imenu menu
;; 	    (imenu-add-to-menubar "imenu")))

;; A number of the erlang-extended-mode key bindings are useful in the shell too
;; (defconst distel-shell-keys
;;   '(("\C-\M-i"   erl-complete)
;;     ("\M-?"      erl-complete)	
;;     ("\M-."      erl-find-source-under-point)
;;     ("\M-,"      erl-find-source-unwind) 
;;     ("\M-*"      erl-find-source-unwind) 
;;     )
;;   "Additional keys to bind when in Erlang shell.")

;; (add-hook 'erlang-shell-mode-hook
;; 	  (lambda ()
;; 	    ;; add some Distel bindings to the Erlang shell
;; 	    (dolist (spec distel-shell-keys)
;; 	      (define-key erlang-shell-mode-map (car spec) (cadr spec)))))


;; http://emacs.wordpress.com/2008/07/18/keeping-your-secrets-secret/ gpg gnupg
(require 'epa)
(epa-file-enable)

;; ;; http://www.emacswiki.org/emacs/aes.el
;; (require 'aes)
;; (aes-enable-auto-decryption)

;; http://xahlee.org/emacs/lisp_regex_replace_func.html should use
;; (interactive "r") to get the selected region see
;; http://xahlee.org/emacs/elisp_basics.html
(defun dictify ()
  "Replace lines of strings with a key/value dict"
  (let (matchedText newText)
    (setq matchedText
          (buffer-substring
           (match-beginning 0) (match-end 0)))
    (setq down
          (downcase matchedText) )
    (setq space
          (replace-regexp-in-string "[ -]" "_" down) )
    (setq punctuation
          (replace-regexp-in-string "[(),/]" "" space) )
    (setq aUm
          (replace-regexp-in-string "ä" "ae" punctuation) )
    (setq oUm
          (replace-regexp-in-string "ö" "oe" aUm) )
    (setq aUm
          (replace-regexp-in-string "ü" "ue" oUm) )
    (setq under
          (replace-regexp-in-string "_+" "_" aUm) )
    under
    )
  )

;; http://exceedhl.wordpress.com/category/emacs/

(defun tweakemacs-delete-region-or-char ()
  "Delete a region or a single character."
  (interactive)
  (if mark-active
      (kill-region (region-beginning) (region-end))
    (delete-char 1)))
(global-set-key (kbd "C-d") 'tweakemacs-delete-region-or-char)

(defun tweakemacs-backward-delete-region-or-char ()
  "Backward delete a region or a single character."
  (interactive)
  (if mark-active
      (kill-region (region-beginning) (region-end))
    (backward-delete-char 1)))
(global-set-key [backspace] 'tweakemacs-backward-delete-region-or-char)

(defun tweakemacs-duplicate-one-line ()
  "Duplicate the current line. There is a little bug: when current line is the last line of the buffer, this will not work as expected. Anyway, that's ok for me."
  (interactive)
  (let ((start (progn (beginning-of-line) (point)))
	(end (progn (next-line 1) (beginning-of-line) (point))))
    (insert-buffer-substring (current-buffer) start end)
    (forward-line -1)))
(global-set-key (kbd "C-=") 'tweakemacs-duplicate-one-line)

(defun tweakemacs-move-one-line-downward ()
  "Move current line downward once."
  (interactive)
  (forward-line)
  (transpose-lines 1)
  (forward-line -1))
(global-set-key [C-M-down] 'tweakemacs-move-one-line-downward)

(defun tweakemacs-move-one-line-upward ()
  "Move current line upward once."
  (interactive)
  (transpose-lines 1)
  (forward-line -2))
(global-set-key [C-M-up] 'tweakemacs-move-one-line-upward)

;; There is a bug in the uncomment-region. When you select
;; the last line of the buffer and if that is a comment,
;; uncomment-region to that region will throw an error: Can't find the comment end.
;; Because I use uncomment-region here, so this command also has this bug.
(defun tweakemacs-comment-dwim-region-or-one-line (arg)
  "When a region exists, execute comment-dwim, or if comment or uncomment the current line according to if the current line is a comment."
  (interactive "*P")
  (if mark-active
      (comment-dwim arg)
    (save-excursion
      (let ((has-comment? (progn (beginning-of-line) (looking-at (concat "\s-*" (regexp-quote comment-start))))))
	(push-mark (point) nil t)
	(end-of-line)
	(if has-comment?
	    (uncomment-region (mark) (point))
	  (comment-region (mark) (point)))))))
(global-set-key (kbd "C-/") 'tweakemacs-comment-dwim-region-or-one-line)

;http://younix.us/cgit/cgit.cgi/dhd.git/tree/hbase/.emacs
; sprunge.us owns
(defun sprunge (prefix)
  "Posts the current buffer to sprunge, and shows the resulting URL in a new buffer"
  (interactive "P")
  (let ((filename "/tmp/sprunge-post"))
    (if prefix (write-file filename) (write-region (region-beginning) (region-end) filename)) ; if invoked with the universal argument / prefix, upload the whole file, else upload just the region
    (insert (shell-command-to-string (concat "curl -s -F 'sprunge=<" filename "' http://sprunge.us")))
    (delete-char -1))) ; Newline after URL

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-safe-themes
   (quote
    ("1e7e097ec8cb1f8c3a912d7e1e0331caeed49fef6cff220be63bd2a6ba4cc365" "e16a771a13a202ee6e276d06098bc77f008b73bbac4d526f160faa2d76c1dd0e" default)))
 '(grep-find-ignored-files
   (quote
    (".#*" "*.hi" "*.o" "*~" "*.bin" "*.lbin" "*.so" "*.a" "*.ln" "*.blg" "*.bbl" "*.elc" "*.lof" "*.glo" "*.idx" "*.lot" "*.fmt" "*.tfm" "*.class" "*.fas" "*.lib" "*.mem" "*.x86f" "*.sparcf" "*.dfsl" "*.pfsl" "*.d64fsl" "*.p64fsl" "*.lx64fsl" "*.lx32fsl" "*.dx64fsl" "*.dx32fsl" "*.fx64fsl" "*.fx32fsl" "*.sx64fsl" "*.sx32fsl" "*.wx64fsl" "*.wx32fsl" "*.fasl" "*.ufsl" "*.fsl" "*.dxl" "*.lo" "*.la" "*.gmo" "*.mo" "*.toc" "*.aux" "*.cp" "*.fn" "*.ky" "*.pg" "*.tp" "*.vr" "*.cps" "*.fns" "*.kys" "*.pgs" "*.tps" "*.vrs" "*.pyc" "*.pyo" "*.map")))
 '(lintnode-location "~/Projects/Haxoring/Node/pkgs/lintnode")
 '(org-agenda-files (quote ("~/timesheet.org" "~/houses.org")))
 '(package-selected-packages
   (quote
    (edit-server company-lsp solarized-theme use-package undo-tree org-plus-contrib magit helm haskell-mode git-timemachine flycheck expand-region web-mode volatile-highlights rainbow-mode rainbow-delimiters pyvirtualenv pymacs pungi php-mode paredit nose multiple-cursors markdown-mode iedit geben fuzzy flymake-python-pyflakes flymake-jslint flymake-cursor fastnav elpy elm-mode coffee-mode buffer-move autopair adaptive-wrap ack-and-a-half ace-jump-mode)))
 '(tool-bar-mode nil)
 '(vc-diff-switches "-b"))


;; (defun get-url (url)
;;   "Retrieve URL, run it through HTML Tidy, and return parsed XML."
;;   ;; (let ((url-package-name (or url-package-name "scrape.el")))
;;   (interactive "sEnter url: ")
;;   ;; (buffer
;;   (let (res-buff (url-retrieve-synchronously url))
;;     (switch-to-buffer res-buff)
;;     )
;;                 ;; (lambda (status)
;;                 ;;   switch-to-buffer (current-buffer))
;;                 ;; )
;; ;; )
;;   ;;   (let ()
;; ;;       ;; (with-current-buffer buffer
;; ;;       ;;   ;; (delete-region (point-min) url-http-end-of-headers)
;; ;;       ;;   ;; (prog1 (scrape-region (point) (point-max))
;; ;;       ;;     (if scrape-debug
;; ;;       ;;         buffer
;; ;;       ;;       (kill-buffer buffer)))
;; ;; )
;;   )
;; ;)
;; ;)


;; Rich's config js2-mode
;; buffer move

(require 'ido)
(ido-mode t)


;; recompile evertying in .emacs.d
;(byte-recompile-directory (expand-file-name "~/.emacs.d") 0)

;; smex suggestions
;; ace jump
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; paste by mouse, at cursor point
(setq mouse-yank-at-point t)

(add-hook
 'desktop-after-read-hook
 '(lambda ()
     (setq frame-title-format   `("%b Desktop: "
                                  ,(mapconcat 'identity (nthcdr 4 (split-string desktop-dirname "/" t)) "/")))))
(defun rgrep-follow-symlinks ()
  "rgrep, but with 'find -L' so that symlinks are followed."
  (interactive)
  (require 'grep)
  (unless grep-find-template
    (grep-compute-defaults))
  (let ((grep-host-defaults-alist nil)
        (grep-find-template (replace-regexp-in-string "\\(find\\).*\\'"
                              "find -L" grep-find-template nil nil 1)))
    (grep-compute-defaults)
    (call-interactively 'rgrep)))
(defalias 'omegrep 'rgrep-follow-symlinks "rgrep for Python egg omelettes.")

(setq nix-indent-function `nix-indent-line)

