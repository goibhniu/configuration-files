{
  packageOverrides = pkgs: rec {
    #myMplayer = (pkgs.lib.overrideDerivation  pkgs.mplayer2 (attrs:{ bs2bSupport = true; };););
    # xsane = (pkgs.xsane.override { gimpSupport = true; });
    #mplayer2 = (pkgs.mplayer2.override { bs2bSupport = true; });
    #myEmacs = (pkgs.emacs.override { withX = false; });
    #myEmacs = (pkgs.emacs.override { gtk = pkgs.gtk3; });
    # envPythonPlonedev = pkgs.buildEnv {
    #   name = "env-python-plonedev-1.0";
    #   ignoreCollisions = true;
    #   paths = with pkgs; [
    #     python27Packages.beautifulsoup
    #     python27Packages.buildout
    #     python27Packages.distutils2
    #     python27Packages.ipython
    #     python27Packages.lxml
    #     python27Packages.pillow
    #     python27Packages.recursivePthLoader
    #     python27Packages.setuptools
    #     gcc
    #   ] ++ (
        
    #     lib.filter (v: (v.type or null) == "derivation")
    #                (lib.attrValues plone42Packages)
    #   );
    # };

    envPyMidi = pkgs.buildEnv {
      name = "env-python-midi-dev-1.0";
      ignoreCollisions = true;
      paths = with pkgs; [
        python27Packages.ipython
        python27Packages.setuptools
        python27Packages.recursivePthLoader
        python27Packages.rtmidi
        python27Packages.evdev
      ];
    };

    envZotonic = pkgs.buildEnv {
      name = "env-zotonic-dev-1.0";
      ignoreCollisions = true;
      paths = with pkgs; [
        erlang
	gcc
	which
#	stdenv
#	glibc
      ];
    };

    zotonic = pkgs.myEnvFun {
      name = "zotonic";
      buildInputs = with pkgs; [
        erlang gcc which  stdenv
      ];
    };


    pythonPlone = pkgs.pythonFull.override {
       extraLibs = with pkgs.python27Packages; [ "Plone-4.3.2" ];
    };

    plone43 = pkgs.myEnvFun {
      name = "plone43";
      buildInputs = with pkgs; [ 
        pythonPlone 
        python27Packages.zc_buildout_nix
        python27Packages.ipython
      ];
      extraCmds = ''
        export PYTHONHOME=${pythonPlone}
        unset http_proxy
      '';
    };

    # plone43 = pkgs.myEnvFun {
    # # unset http_proxy to avoid "Resolving nodtd.invalid... failed: No such file or directory."
    #   name = "plone43-new";
    #   buildInputs = with pkgs; [
    #     pythonPlone
	#     python27Packages.zc_buildout_nix
    #     python27Packages.recursivePthLoader
    #     # python27Packages.pillow
    #     # python27Packages.jedi
    #     # python27Packages.epc
    #     stdenv # provides gcc for building python c extensions
    #  ];
    # };

  };
} 


